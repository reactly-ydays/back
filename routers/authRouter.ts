import { Router } from "express"
import { login, register, changeToken, logout} from "../controllers/authController"

const authRouter = Router()

// Route login
authRouter.post('/login', login)

// Route register
authRouter.post('/register', register)

// Route resfresh token
authRouter.post('/token', changeToken)

// Route logout
authRouter.post('/logout', logout)

export default authRouter
