import { NextFunction, Request, Response, Router } from "express"
import HttpException from "../errors/ErrorFormat"
import postRouter from "./postRouter"
import userRouter from "./userRouter"
import authRouter from "./authRouter"
import teamRouter from "./teamRouter"
import kanbanRouter from "./kanbanRouter"

const router = Router()
router.use('/posts', postRouter)
router.use('/users', userRouter)
router.use('/auth', authRouter)
router.use('/teams', teamRouter)
router.use('/kanban', kanbanRouter)

router.get('/', (req: Request, res: Response, next: NextFunction) => {  
    next(new HttpException(406, "API_NOT_AVAILABLE", "Route doesn't exist"))
})

export default router
