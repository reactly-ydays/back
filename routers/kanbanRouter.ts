import { Router } from "express"
import { allTables, addTable, addSubTable, addTask, moveTask, editSubTable, deleteSubTable, editTask, deleteTask, deleteTable, editTable } from "../controllers/kanbanController"

const kanbanRouter = Router()

// Route all tables by teamId
kanbanRouter.get('/team/:teamId', allTables)

// Route add table
kanbanRouter.post('/team/:teamId', addTable)

// Route update table
kanbanRouter.put('/table/:tableId', editTable)

// Route delete table
kanbanRouter.delete('/table/:tableId', deleteTable)

// Route add sub table
kanbanRouter.post('/table/:tableId', addSubTable)

// Route add task
kanbanRouter.post('/subTable/:subTableId', addTask)

// Route move task
kanbanRouter.put('/task/:taskId', moveTask)

// Route edit task
kanbanRouter.put('/task/:taskId/edit', editTask)

// Route delete task
kanbanRouter.delete('/task/:taskId', deleteTask)

// Route update sub table
kanbanRouter.put('/subTable/:subTableId', editSubTable)

// Route delete sub table
kanbanRouter.delete('/subTable/:subTableId', deleteSubTable)

export default kanbanRouter
