import { Router } from "express"
import { checkToken } from "../middleware/auth"
import path from "path"
import crypto from "crypto"
import { addPosts, getAllPosts, deletePosts, getAllUserPosts, addComment, getPost, updateLike, removeComment } from "../controllers/postsController"
import multer from "multer"
import HttpException from "../errors/ErrorFormat"

const postRouter = Router()

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, path.join(__dirname, "../uploads/posts/"))
    },
    filename: function(req, file, cb) {
        cb(null, crypto.randomBytes(64).toString("hex") + path.extname(file.originalname))
    }
})

const fileFilter = (req: any, file: any, cb: any) => {
    if (file.mimetype === "image/jpeg" || file.mimetype === "image/jpg" || file.mimetype === "image/png" || file.mimetype === "image/gif") {
        cb(null, true)
    } else {
        cb(new HttpException(400, 'BAD_REQUEST', "Invalid image type"), false)
    }
}

const upload = multer({
    storage: storage,
    limits: { fieldSize: 1024 * 1024 * 5 }, 
    fileFilter: fileFilter 
})


// Route get all posts
postRouter.get('/', checkToken, getAllPosts)

// Route get a post
postRouter.get('/:id', checkToken, getPost)

// Route add post
postRouter.post('/', upload.array("images"), checkToken, addPosts)

// Route add comment
postRouter.post("/:id/comment", checkToken, addComment)

// Route add comment
postRouter.delete("/:id/comment/:commentId", checkToken, removeComment)

// Route add or remove like
postRouter.patch("/:id/like", checkToken, updateLike)

// Route delete post
postRouter.delete('/:id', checkToken, deletePosts)

// Route get All post of a user
postRouter.get('/users/:id', checkToken, getAllUserPosts)

export default postRouter
