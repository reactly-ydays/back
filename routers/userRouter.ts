import { Router } from "express"
import path from "path"
import crypto from "crypto"
import multer from "multer"
import fs from "fs"
import HttpException from "../errors/ErrorFormat"
import { getOneUser, getAllTeamUsers, patchChangeUserTeam, deleteUser, deleteTeam, patchUser } from "../controllers/usersController"
import { checkToken, getUserId } from "../middleware/auth"
import User from "../models/User"
import { BASE_URL } from "../server"

const userRouter = Router()

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, path.join(__dirname, "../uploads/users/"))
    },
    filename: function(req, file, cb) {
        User.findOne({ _id: getUserId.userId }, (err, user) => {
            if (err) return new HttpException(500, "SERVER_ERROR", "Unknow error")
            if (user?.image !== null) {
                if (user?.image.includes(BASE_URL)) {
                    const removeBaseUrl = new RegExp(BASE_URL, "g")
                    fs.unlinkSync(path.join(__dirname, `..${user?.image.replace(removeBaseUrl, "")}`))
                }
            }
        })
        cb(null, crypto.randomBytes(64).toString("hex") + path.extname(file.originalname))
    }
})

const fileFilter = (req: any, file: any, cb: any) => {
    if (file.minetype === "image/jpeg" || file.minetype === "image/jpg" || file.minetype === "image/png") {
        cb(null, true)
    } else {
        cb(null, new HttpException(400, 'BAD_REQUEST', "Invalid image type"))
    }
}

const upload = multer({
    storage: storage,
    limits: { fieldSize: 1024 * 1024 * 5 }, 
    fileFilter: fileFilter 
})

// Route get a user
userRouter.get('/', checkToken, getOneUser)

// Route user changes 
userRouter.patch('/', checkToken, upload.single("image"), patchUser)

// Route get all user in a team
userRouter.get('/teams/:team', checkToken,getAllTeamUsers)

// Route user changes team
userRouter.patch('/teams/:team', checkToken, patchChangeUserTeam)

// Route delete user
userRouter.delete('/', checkToken, deleteUser)

// Route delete team
userRouter.delete('/teams/:team', checkToken, deleteTeam)

export default userRouter
