import { Router } from "express"
import { allTeams, addTeam } from "../controllers/teamController"

const teamRouter = Router()

// Route all teams
teamRouter.get('/', allTeams)

// Route all teams
teamRouter.post('/:team', addTeam)

export default teamRouter
