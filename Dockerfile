FROM node:14.14.0-alpine
RUN apk update
WORKDIR /api
COPY . ./

RUN apk add --update nodejs npm

ENV JWT_SECRET=jwt-secret
ENV JWT_REFRESH_SECRET=jwt-refresh-secret
ENV JWT_EXPIRES_IN=20m
ENV DB_URL=mongodb+srv://reactly:reactly@reactly-cluster.3iyy3.mongodb.net/reactly?retryWrites=true&w=majority

EXPOSE 80

RUN npm install
CMD npm start
