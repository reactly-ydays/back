import express, { Request, Response, NextFunction } from "express"
import path from "path"
import cors from "cors"
import expressJwt from "express-jwt"
import bodyParser from "body-parser"
import dotenv from "dotenv"
import router from "./routers/routes"
import HttpException, { ErrorFormat } from "./errors/ErrorFormat"
import UserFixtures from "./fixtures/UserFixtures"
import NbLikeFixtures from "./fixtures/NbLikeFixtures"

const app = express()
dotenv.config()
const port = process.env.APP_PORT || 8080
export const appRoot = path.resolve(__dirname)
export const BASE_URL = "https://api.reactly.fr"

export const jwtSecret = Buffer.from(`${process.env.JWT_SECRET}`, 'base64')
export const jwtRefreshTokenSecret = Buffer.from(`${process.env.JWT_REFRESH_SECRET}`, 'base64')

app.use((req, res, next) => { next() }, cors({ origin: true }))
app.use("/uploads", express.static("uploads"))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use('/', router)
app.use(expressJwt({ secret: jwtSecret, algorithms: ['HS256'] }).unless({ path: [ "/auth/login", "/auth/register", "/", "/uploads/users/" ]}))

app.use((req: Request, res: Response, next: NextFunction) => {
  res.setHeader('Access-Control-Allow-Origin', 'x-www-form-urlencoded, Origin, X-Requested-With, Content-Type, Accept, Authorization, *')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
  next()
})

app.use((err: HttpException, req: Request, res: Response, next: NextFunction) => {
  ErrorFormat(res, err)
})

app.listen(port, () => {
  UserFixtures.then(() => {
    NbLikeFixtures()
    console.log("Database seeded :)")
  })
  console.log(`Server started on port ${port}`)
})

