import { Request, Response, NextFunction } from "express"
import jwt from "jsonwebtoken"
import { jwtSecret } from "../server"
import HttpException from "../errors/ErrorFormat"

export const getUserId = { userId: null }

export const checkToken = (req: Request, res: Response, next: NextFunction) => {
    const token: any = req.headers["x-reactly-auth-token"] || req.headers["X-REACTLY-AUTH-TOKEN"]

    if (!token) {
        getUserId.userId = null
        return next(new HttpException(401, "UNAUTHORIZED", "Token not found"))
    }


    jwt.verify(token, jwtSecret, (err: any, decodedToken: any) => {
        if (err) {
            getUserId.userId = null
            if (err.name === "TokenExpiredError") {
                return next(new HttpException(401, "UNAUTHORIZED", "Token expired"))
            } 
            return next(new HttpException(401, "UNAUTHORIZED", "Token not valid"))
        } 
        getUserId.userId = decodedToken.id
        return next()
    })
}
