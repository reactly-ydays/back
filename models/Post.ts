import mongo from "../db/Mongo"
import { Document } from "mongoose"
const { Schema } = mongo

export type UserForPostType = {
    id: String,
    name: String,
    job : String,
    image: String 
}

export type CommentsForPostType = {
    id: string,
    userId: string,
    comment: string,
    user: UserForPostType,
    publicationDate: Date
}

export const PostType = {
    user: Object,
    nbLike: Number,
    description: String,
    images: {
        type: Array,
        default: [],
        required: false
    },
    comments : {
        type: Array,
        default: [],
        required: false
    },
    publicationDate: Date,
    currentUserLiked: Boolean
}

export interface IPostDoc extends Document {
    user: UserForPostType,
    nbLike: number,
    description: string,
    images: string[],
    comments : Array<CommentsForPostType>,
    publicationDate: Date,
    currentUserLiked: Boolean
}

const postSchema = new Schema(PostType, { versionKey: false })
const Post = mongo.model<IPostDoc>('Post', postSchema, 'Post')

export default Post
