import bcrypt from "bcrypt"
import mongo from "../db/Mongo"
import { Document } from "mongoose"
const { Schema } = mongo
const SALT_WORK_FACTOR = 10

export const UserType = {
    email: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    team: {
        type: String,
        default: null,
        required: false
    },
    teamId: {
        type: String,
        default: null,
        required: false
    },
    job: {
        type: String,
        required: true
    },
    image: {
        type: String,
        default: null,
        required: false
    }
}

export interface IUserDoc extends Document {
    email: string,
    firstName: string,
    lastName: string,
    password: string,
    team: string | null,
    teamId: string | null,
    job: string,
    image: string | null,
    comparePassword: (arg0: string, arg1: CallableFunction) => CallableFunction
}

const userSchema = new Schema(UserType, { versionKey: false })

userSchema.pre('save', function(next: Function) {
    const user: any = this
    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next()

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
        if (err) return next(err)

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, (err, hash) => {
            if (err) return next(err)
            // override the cleartext password with the hashed one
            user.password = hash
            next()
        })
    })
})
     
userSchema.methods.comparePassword = function(candidatePassword: string, cb: CallableFunction) {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
        if (err) return cb(err)
        cb(null, isMatch)
    })
}

const User = mongo.model<IUserDoc>('User', userSchema, 'User')

export default User
