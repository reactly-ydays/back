import mongo from "../db/Mongo"
import { Document } from "mongoose"
const { Schema } = mongo

export type UserForTaskType = {
    id: String,
    name: String,
    job : String,
    image: String 
}

export const TaskType = {
    title: String,
    description: String,
    subTableId: String,
    user: Object
}

export interface ITaskDoc extends Document {
    title: string,
    description: string,
    subTableId: string,
    user: UserForTaskType
}

const taskSchema = new Schema(TaskType, { versionKey: false })
const Task = mongo.model<ITaskDoc>('Task', taskSchema, 'Task')

export default Task
