import mongo from "../db/Mongo"
import { Document } from "mongoose"
const { Schema } = mongo

export const SubTableType = {
    name: String,
    tableId: String
}

export interface ISubTableDoc extends Document {
    name: string,
    tableId: string
}

const subTableSchema = new Schema(SubTableType, { versionKey: false })
const SubTable = mongo.model<ISubTableDoc>('SubTable', subTableSchema, 'SubTable')

export default SubTable
