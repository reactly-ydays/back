import mongo from "../db/Mongo"
import { Document } from "mongoose"
const { Schema } = mongo

export const TeamType = {
   name: String
}

export interface ITeamDoc extends Document {
    name: string
}

const teamSchema = new Schema(TeamType, { versionKey: false })
const Team = mongo.model<ITeamDoc>('Team', teamSchema, 'Team')

export default Team
