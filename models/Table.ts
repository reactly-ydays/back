import mongo from "../db/Mongo"
import { Document } from "mongoose"
const { Schema } = mongo

export const TableType = {
    name: String,
    teamId: String
}

export interface ITableDoc extends Document {
    name: string,
    teamId: string
}

const tableSchema = new Schema(TableType, { versionKey: false })
const Table = mongo.model<ITableDoc>('Table', tableSchema, 'Table')

export default Table
