import mongo from "../db/Mongo"
import { Document } from "mongoose"
const { Schema } = mongo

export const NbLikeType = {
    postId: String,
    userIdOwner: String,
    userIdLike: Array
}

export interface INbLikeDoc extends Document {
    postId: string,
    userIdOwner: string
    userIdLike: string[]
}

const nbLikeSchema = new Schema(NbLikeType, { versionKey: false })
const NbLike = mongo.model<INbLikeDoc>('NbLike', nbLikeSchema, 'NbLike')

export default NbLike
