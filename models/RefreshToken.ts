import mongo from "../db/Mongo"
import { Document } from "mongoose"
const { Schema } = mongo

export const RefreshTokenType = {
    id: String,
    refreshToken: String
}

export interface IRefreshTokenDoc extends Document {
    id: string,
    refreshToken: string
}

const refreshTokenSchema = new Schema(RefreshTokenType, { versionKey: false })
const RefreshToken = mongo.model<IRefreshTokenDoc>('RefreshToken', refreshTokenSchema, 'RefreshToken')

export default RefreshToken
