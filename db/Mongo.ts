import mongoose from "mongoose"
import dotenv from "dotenv"
dotenv.config()
const mongo = mongoose

const url = `${process.env.DB_URL || 'mongodb://localhost:27017/reactly'}`

mongo.connect(url, { 
    useNewUrlParser: true,
    useUnifiedTopology: true 
})
  
const conn = mongo.connection

conn.on("open", () => { 
    console.log('Database connected !') 
})

conn.on('error', (err: Error) => { 
    console.log('Connection database error: ', err)
})

export default mongo
