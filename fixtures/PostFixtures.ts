import User from "../models/User"
import Post from "../models/Post"
import faker from "faker"
import { capitalize, getRandomInteger } from "../utils/Utils"
import NbLike from "../models/NbLike"
faker.locale = "fr"

const PostFixtures = (userId: string) => {
    User.findOne({ _id: userId }, (err, user) => {
        if (err) return err

        const description = (getRandomInteger(1, 2) === 1) ? faker.lorem.sentences(getRandomInteger(1, 3)) : faker.lorem.paragraphs(getRandomInteger(1, 3))
        const publicationDate = new Date(faker.date.recent(7, new Date()))

        const newPost = new Post({
            user: {
                id: userId,
                name: `${user!.firstName} ${user!.lastName}`,
                job : `${capitalize(user!.team)} - ${user!.job}`,
                image: user!.image 
            },
            nbLike: 0,
            description,
            images: null,
            comments: [],
            publicationDate,
            currentUserLiked: false
        })
        newPost.save((err, post) => {
            if (err) return err
            const nbLikeValue = new NbLike({  postId: post._id, userIdOwner: userId, userIdLike: [] })
            nbLikeValue.save()
        })
    })
}

export default PostFixtures