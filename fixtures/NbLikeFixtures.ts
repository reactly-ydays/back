import User from "../models/User"
import faker from "faker"
import { getRandomInteger, shuffle } from "../utils/Utils"
import Post from "../models/Post"
import NbLike from "../models/NbLike"
faker.locale = "fr"

const NbLikeFixtures = () => {
    const allPost = Post.find({})
    const allUser = User.find({})

    allUser.exec((err, users) => {
        if (err) return err
        for (let i = 0; i < users.length; i++) {
            allPost.exec(async (err, posts) => {
                if (err) return err

                const shufflePosts = shuffle(posts)

                for(let j = 0; j < getRandomInteger(1, shufflePosts.length); j++) {
                   await NbLike.findOne({ postId: shufflePosts[j]._id }, (err, nbLike) => {
                        if (err || nbLike === null) return err
                        NbLike.updateOne({ postId: shufflePosts[j]._id }, { $push: { userIdLike: { $each: [ `${users[i]._id}` ] } } }, (err) => {
                            if (err) return err
                        
                            Post.updateOne({ _id: shufflePosts[j]._id }, { nbLike: nbLike?.userIdLike.length + 1 }, (err) => {
                                if (err) return err
                            })
                        })
                    })
                }
            })
        }
    })
}

export default NbLikeFixtures