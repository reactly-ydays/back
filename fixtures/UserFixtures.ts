import User from "../models/User"
import faker from "faker"
import PostFixtures from "./PostFixtures"
import { getRandomColor, getRandomInteger } from "../utils/Utils"
import Team, { ITeamDoc } from "../models/Team"
import TeamFixture from "./TeamFixtures"
faker.locale = "fr"

const UserFixtures = new Promise<void>((resolve) => {
    User.find({}, (err, users) => {
        if (err) return err

        TeamFixture.then(() => {
            if (users.length < 25) {
                Team.find({}, (err, teams: ITeamDoc[]) => {
                    if (err) return err
                    const newUser = new User({
                        email: "lucas.consejo@gmail.com",
                        firstName: "Lucas",
                        lastName: "Consejo",
                        password: "test",
                        team: "Équipe Recherche",
                        teamId: teams[0]._id,
                        job: "Développeur",
                        image: `https://eu.ui-avatars.com/api/?background=${getRandomColor()}&color=fff&name=Lucas+Consejo&size=512&bold=true`
                    })
                    newUser.save()
    
                    for (let i = 0; i < 25; i++) {
                        const randomTeam = getRandomInteger(0, teams.length-1)
                        const firstName = faker.name.firstName()
                        const lastName = faker.name.lastName()
                        const email = `${firstName}.${lastName}@test.com`                
                        const job = faker.name.jobType()
                        const team = `Équipe ${teams[randomTeam].name}`
                        const teamId = teams[randomTeam]._id
                        const image = `https://eu.ui-avatars.com/api/?background=${getRandomColor()}&color=fff&name=${firstName}+${lastName}&size=512&bold=true`
                        const newUser = new User({
                            email: email.toLowerCase(),
                            firstName,
                            lastName,
                            password: "test",
                            team,
                            teamId,
                            job,
                            image
                        })
                        newUser.save((err, user) => {
                            if (err) return err
                            PostFixtures(user._id)
                        })
                    }
                    setTimeout(() => {
                        resolve()
                    }, 1000)
                })
            }
        })

    })
})

export default UserFixtures