import Team from "../models/Team"

const TeamFixture = new Promise<void>((resolve) => {
    const teams = [
        { name: "Recherche" },
        { name: "OPS" },
        { name: "DevOps" },
        { name: "Offre" },
        { name: "Produit" },
        { name: "Catalogue" },
        { name: "Securité" },
        { name: "Marketing" },
        { name: "Communication" },
        { name: "Direction" },
        { name: "SAV" }
    ]

    Team.find({}, (err, allTeams) => {
        if (err) return err
        if (!allTeams.length) {
            Team.insertMany(teams).then(() => resolve())
        }
    })
})

export default TeamFixture