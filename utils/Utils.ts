export const capitalize = (value: string | null) => {
    if(value === null) {
        return ""
    }
    return value![0].toUpperCase() + value!.slice(1)
}

export const isEmpty = (value: any) => {
    return (value === undefined || value === null || value === "" || typeof value === "object" && Object.keys(value).length === 0 || Array.isArray(value) && value.length === 0 || value.length === 0)
}

export const strTrim = (str: string) => {
    return str.trim()
}

export const lengthEqualsTo = (str: string, length: number = 0) => {
    return strTrim(str).length === length
}

export const lengthLargerThan = (str: string, length: number = 0) => {
    return strTrim(str).length > length
}

export const lengthSmallerThan = (str: string, length: number = 0) => {
    return strTrim(str).length < length
}

export const getRandomInteger = (min: number, max: number) => {
    return Math.floor(Math.random() * (max - min + 1)) + min
}

export const shuffle = (array: Array<any>) => {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex)
      currentIndex -= 1
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex]
      array[currentIndex] = array[randomIndex]
      array[randomIndex] = temporaryValue
    }
    return array
}

export const getRandomColor = () => {
    return Math.floor(Math.random()*16777215).toString(16)
}