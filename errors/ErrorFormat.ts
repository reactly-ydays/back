import { Response } from 'express'

class HttpException extends Error {
  status: number
  message: string
  type: string
  constructor(status: number, type: string, message: string) {
      super(message)
      this.status = status
      this.message = message
      this.type = type
  }
}

export const ErrorFormat = (res: Response, err: HttpException) => {
  const { status, type, message } = err
  res.status(status).json({
    status: "error",
    type: type,
    message: message
  })
}

export default HttpException
