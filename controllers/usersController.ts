import { Request, Response, NextFunction } from "express"
import HttpException from "../errors/ErrorFormat"
import fs from "fs"
import path from "path"
import User from "../models/User"
import Post from "../models/Post"
import { getUserId } from "../middleware/auth"
import { appRoot, BASE_URL } from "../server"
import NbLike from "../models/NbLike"
import { ObjectId } from "mongodb"

// Route get a user
export const getOneUser = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id
    const { userId } = getUserId

    User.findById(userId, { password: 0 }, (err, user) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        res.status(200).json({ status: "success", message: "Get user" , data: user })
    })
}

// Route get all user in a team
export const getAllTeamUsers = (req: Request, res: Response, next: NextFunction) => {
    const team = req.params.team

    User.find({ "team": team }, { password: 0 }, (err, users) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        res.status(200).json({ status: "success", message: "Get all User posts", data: users })
    })
}

// Route user changes 
export const patchUser = (req: Request, res: Response, next: NextFunction) => {
    const { firstName, lastName, job } = req.body
    const { userId } = getUserId

    let image: string | null = null
    if (req.file !== undefined) {
        image = `${BASE_URL}${req.file.path.split(appRoot)[1]}`
    }

    User.updateOne({ _id: userId }, { firstName, lastName, image, job }, (err) => {
        if (err) {
            fs.unlinkSync(req.file.path)
            return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        }
        User.findById(userId, { password: 0 }, (err, user) => {
            if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
            res.status(200).json({ status: "success", message: "User changed" , data: user })
        })
    })
}

// Route user changes team
export const patchChangeUserTeam = (req: Request, res: Response, next: NextFunction) => {
    const team = req.params.team
    const { userId } = getUserId

    User.updateOne({ _id: userId }, { '$set': { 'team' : team } }, (err) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        User.findById(userId, { password: 0 }, (err, user) => {
            if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
            res.status(200).json({ status: "success", message: "User changed team" , data: user })
        })
    })
}

// Route delete user
export const deleteUser = (req: Request, res: Response, next: NextFunction) => {
    const { userId } = getUserId

    User.findOne({ _id: userId }, (err, user) => {
        if (err) return new HttpException(500, "SERVER_ERROR", "Unknow error")
        if (user?.image !== null) {
            if (user?.image.includes(BASE_URL)) {
                const removeBaseUrl = new RegExp(BASE_URL, "g")
                fs.unlinkSync(path.join(__dirname, `..${user?.image.replace(removeBaseUrl, "")}`))
            }
        }

        User.deleteOne({ _id: userId }, (err) => {
            if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
    
            Post.deleteMany({ "user.id": new ObjectId(`${userId}`) }, (err) => {
                if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))

                Post.updateMany({ "comments.user.id": userId }, { $pull: { comments: { userId: { $in: [`${userId}`] } } } }, (err) => {
                    if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                    
                    NbLike.updateMany({ userIdLike: { "$in" : [`${userId}`] } }, { $pullAll: { userIdLike: [ `${userId}` ] } }, (err) => {
                        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        
                        Post.updateMany({ nbLike: { "$lt": 0 } }, { nbLike: 0 }, (err) => {
                            if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
    
                            NbLike.deleteMany({ userIdOwner: `${userId}`}, (err) => {
                                if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        
                                res.status(200).json({ status: "success", message: "User deleted" })
                            })
                        })
                    })
                })
            })
        })
    })
}

// Route delete team
export const deleteTeam = (req: Request, res: Response, next: NextFunction) => {
    const team = req.params.team

    User.updateMany({ team: team }, { '$set': { 'team' : null } }, { multi: true }, (err) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        res.status(200).json({ status: "success", message: "Team deleted" })
    })
}
