import { Request, Response, NextFunction } from "express"
import HttpException from "../errors/ErrorFormat"
import Table from "../models/Table"
import SubTable from "../models/SubTable"
import Task from "../models/Task"
import { getUserId } from "../middleware/auth"
import User from "../models/User"
import { capitalize } from "../utils/Utils"

// Route all tables by teamId
export const allTables = (req: Request, res: Response, next: NextFunction) => {
    const { teamId } = req.params

    const allTable = Table.find({ teamId })

    allTable.exec(async (err, tables) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        
        let newTable: any = []
        for(let i = 0; i < tables.length; i++) {
            const newSubTable: any = []
            const newTask: any = []

            await SubTable.find({ tableId: tables[i]._id }).then(async res => {
                for(let j = 0; j < res.length; j++) {
                    newSubTable.push(res[j])

                    await Task.find({ subTableId: res[j]._id }).then(res => {
                        for(let k = 0; k < res.length; k++) {
                            newTask.push(res[k])
                        }
                    })
                }
            })
            newTable.push({
                _id: tables[i]._id,
                name: tables[i].name,
                subTable: newSubTable,
                tasks: newTask
            })
        }

        res.status(200).json({ status: "success", message: "Get all tables by teamId", data: newTable })
    })
}

// Route add table
export const addTable = (req: Request, res: Response, next: NextFunction) => {
    const { name } = req.body
    const { teamId } = req.params
    
    const table = new Table({
       name,
       teamId
    })

    table.save((err, table) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
       
        const newTable = {
            _id: table._id,
            name: table.name,
            teamId,
            subTable: [],
            tasks: []
        }
        res.status(201).json({ status: "success", message: "Table created" , data: newTable })
    })
}

// Route add sub table
export const addSubTable = (req: Request, res: Response, next: NextFunction) => {
    const { name } = req.body
    const { tableId } = req.params
    
    const subTable = new SubTable({
       name,
       tableId
    })

    subTable.save((err, subTable) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
       
        res.status(201).json({ status: "success", message: "SubTable created" , data: subTable })
    })
}

// Route add task
export const addTask = (req: Request, res: Response, next: NextFunction) => {
    const { title, description } = req.body
    const { subTableId } = req.params
    const { userId } = getUserId

    User.findById(userId, { password: 0 }, (err, user) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        if (!user) return new HttpException(404, "NOT_FOUND", "user not found")

        const task = new Task({
            title,
            description,
            subTableId,
            user: {
                id: userId,
                name: `${user!.firstName} ${user!.lastName}`,
                job : `${capitalize(user!.team)} - ${user!.job}`,
                image: user!.image 
            },
        })
    
        task.save((err, task) => {
            if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        
            res.status(201).json({ status: "success", message: "Task created" , data: task })
        })
    })
}

// Route move task
export const moveTask = (req: Request, res: Response, next: NextFunction) => {
    const { newSubTableId } = req.body
    const { taskId } = req.params

    Task.updateOne({ _id: taskId }, { '$set': { 'subTableId': newSubTableId } }, (err) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        res.status(200).json({ status: "success", message: "Task moved" , data: [] })
    })
}

// Route update sub table
export const editSubTable = (req: Request, res: Response, next: NextFunction) => {
    const { name } = req.body
    const { subTableId } = req.params

    SubTable.updateOne({ _id: subTableId }, { '$set': { 'name': name } }, (err) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))

        SubTable.findOne({ _id: subTableId }, (err, subTable) => {
            res.status(200).json({ status: "success", message: "Sub table updated" , data: subTable })
        })
    })
}

// Route delete sub table
export const deleteSubTable = (req: Request, res: Response, next: NextFunction) => {
    const { subTableId } = req.params

    SubTable.deleteOne({ _id: subTableId }, (err) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        
        res.status(200).json({ status: "success", message: "Sub table deleted" })
    })
}

// Route update task
export const editTask = (req: Request, res: Response, next: NextFunction) => {
    const { title, description } = req.body
    const { taskId } = req.params

    Task.updateOne({ _id: taskId }, { '$set': { 'title': title, 'description': description } }, (err) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        res.status(200).json({ status: "success", message: "Task updated" , data: [] })
    })
}

// Route delete task
export const deleteTask = (req: Request, res: Response, next: NextFunction) => {
    const { taskId } = req.params

    Task.deleteOne({ _id: taskId }, (err) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        res.status(200).json({ status: "success", message: "Task deleted" , data: [] })
    })
}

// Route update table
export const editTable = (req: Request, res: Response, next: NextFunction) => {
    const { name } = req.body
    const { tableId } = req.params

    Table.updateOne({ _id: tableId }, { '$set': { 'name': name } }, (err) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))

        Table.findOne({ _id: tableId }, (err, table) => {
            res.status(200).json({ status: "success", message: "Table updated" , data: table })
        })
    })
}

// Route delete table
export const deleteTable = (req: Request, res: Response, next: NextFunction) => {
    const { tableId } = req.params

    const table = Table.findOne({ _id: tableId })

    table.exec(async (err, t) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))

        await SubTable.find({ tableId: t?._id }).then(async res => {
            for(let j = 0; j < res.length; j++) {
                await Task.deleteMany({ subTableId: res[j]._id })
            }
        })
        await SubTable.deleteMany({ tableId: t?._id })
    })

    Table.deleteOne({ _id: tableId }, (err) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        res.status(200).json({ status: "success", message: "Table deleted" })
    })
}