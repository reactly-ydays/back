import { Request, Response, NextFunction } from "express"
import { v4 as uuidv4 } from "uuid"
import HttpException from "../errors/ErrorFormat"
import { getUserId } from "../middleware/auth"
import fs from "fs"
import path from "path"
import Post, { CommentsForPostType } from "../models/Post"
import User from "../models/User"
import { appRoot, BASE_URL } from "../server"
import { capitalize, isEmpty, strTrim } from "../utils/Utils"
import NbLike from "../models/NbLike"

// Route get all posts
export const getAllPosts = (req: Request, res: Response, next: NextFunction) => {
    const { userId } = getUserId
    const page: number = Number(req.query.page)
    const limit = 10

    const allPost = Post.find({}).sort({"publicationDate": -1}).limit(limit).skip(limit * page)

    allPost.exec(async (err, posts) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))

        const newPosts: any = []
        for(let i = 0; i < posts.length; i++) {
            await NbLike.findOne({ postId: posts[i]._id }).then(res => {
                if (res === null) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                
                const newPost: any = posts[i]
                
                if (res.userIdLike.includes(`${userId}`)) {
                    newPost.currentUserLiked = true
                } else {
                    newPost.currentUserLiked = false
                }
                newPosts.push(newPost)
            })
        }
        
        const total = await Post.countDocuments({})
        const hasNextPage = (total !== ((limit * page) + posts.length))

        res.status(200).json({ status: "success", message: "Get all Post", data: newPosts, hasNextPage })
    })
}

// Route get a post
export const getPost =  (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id
    const { userId } = getUserId

    const getPost = Post.findOne({ _id: id })

    getPost.exec(async (err, post) => {
        if(err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        if(!post || post === null) return next(new HttpException(404, "NOT_FOUND", "post not found"))
        
        let newPost: any = post

        await NbLike.findOne({ postId: post._id }, (err, nbLike) => {
            if (err || nbLike === null) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
            if (nbLike?.userIdLike.includes(`${userId}`)) {
                newPost.currentUserLiked = true
            } else {
                newPost.currentUserLiked = false
            }
        })

        res.status(200).json({ status: "success", message: "Get a Post", data: newPost })
    })
}

// Route add post
export const addPosts = (req: any, res: Response, next: NextFunction) => {
    const { description } = req.body
    const { userId } = getUserId

    let images: string[] = []
    if (req.files !== undefined) {
        for(let i = 0; i < req.files.length; i++) {
            images?.push(`${BASE_URL}${req.files[i].path.split(appRoot)[1]}`)
        }
    }

    User.findById(userId, { password: 0 }, (err, user) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        if (!user) return new HttpException(404, "NOT_FOUND", "user not found")
        const post = new Post({
            user: {
                id: userId,
                name: `${user!.firstName} ${user!.lastName}`,
                job : `${capitalize(user!.team)} - ${user!.job}`,
                image: user!.image 
            },
            nbLike: 0,
            description,
            images,
            comments: [],
            publicationDate: new Date(),
            currentUserLiked: false
        })

        post.save((err, post) => {
            if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
            const nbLikeValue = new NbLike({  postId: post._id, userIdOwner: userId, userIdLike: [] })

            nbLikeValue.save((err) => {
                if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                res.status(201).json({ status: "success", message: "Post created" , data: post })
            })
        })
    })
}

// Route add or remove like 
export const updateLike = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id
    const { userId } = getUserId

    NbLike.findOne({ postId: id }, (err, nbLike) => {
        if (err || nbLike === null) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        if (nbLike?.userIdLike.includes(`${userId}`)) {
            NbLike.updateOne({ postId: id }, { $pullAll: { userIdLike: [ `${userId}` ] } }, (err) => {
                if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))

                const newNbLike = (nbLike?.userIdLike.length - 1 <= 0) ? 0 : nbLike?.userIdLike.length - 1

                Post.updateOne({ _id: id }, { nbLike: newNbLike }, (err) => {
                    if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))

                    const getPost = Post.findOne({ _id: id })

                    getPost.exec(async (err, post) => {
                        if(err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                        if(!post || post === null) return next(new HttpException(404, "NOT_FOUND", "post not found"))
                        
                        let newPost: any = post

                        await NbLike.findOne({ postId: post._id }, (err, nbLike) => {
                            if (err || nbLike === null) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                            if (nbLike?.userIdLike.includes(`${userId}`)) {
                                newPost.currentUserLiked = true
                            } else {
                                newPost.currentUserLiked = false
                            }
                        })
                        
                        res.status(200).json({ status: "success", message: "Post unliked", data: newPost })
                    })
                })
            })
        } else {
            NbLike.updateOne({ postId: id }, { $push: { userIdLike: { $each: [ `${userId}` ] } } }, (err) => {
                if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))

                Post.updateOne({ _id: id }, { nbLike: nbLike?.userIdLike.length + 1 }, (err) => {
                    if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                    
                    const getPost = Post.findOne({ _id: id })

                    getPost.exec(async (err, post) => {
                        if(err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                        if(!post || post === null) return next(new HttpException(404, "NOT_FOUND", "post not found"))
                        
                        let newPost: any = post

                        await NbLike.findOne({ postId: post._id }, (err, nbLike) => {
                            if (err || nbLike === null) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                            if (nbLike?.userIdLike.includes(`${userId}`)) {
                                newPost.currentUserLiked = true
                            } else {
                                newPost.currentUserLiked = false
                            }
                        })

                        res.status(200).json({ status: "success", message: "Post liked", data: newPost })
                    })
                })
            })
        }
    })

}

// Route add comment
export const addComment = (req: Request, res: Response, next: NextFunction) => {
    const comment: string = req.body.comment
    const id = req.params.id 
    const { userId } = getUserId

    if (isEmpty(strTrim(comment))) return next(new HttpException(400, "BAD_REQUEST", "missing_fields"))
    if (id.trim().length < 24) return next(new HttpException(404, "NOT_FOUND", "Post not found"))

    User.findById(userId, { password: 0 }, (err, user) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        if (!user) return new HttpException(404, "NOT_FOUND", "user not found")

        const commentValue: CommentsForPostType = {
            id: `${uuidv4().toString()}`,
            comment: comment,
            userId: `${userId}`,
            user: {
                id: `${userId}`,
                name: `${user!.firstName} ${user!.lastName}`,
                job : `${capitalize(user!.team)} - ${user!.job}`,
                image: `${user!.image}` 
            },
            publicationDate: new Date()
        }

        Post.findOne({ _id: id }, (err, post) => {
            if(err) return new HttpException(500, "SERVER_ERROR", "Unknow error")
            if(!post) return new HttpException(404, "NOT_FOUND", "Post not found")

            Post.updateOne({ _id: id }, { $push: { comments: { $each : [ commentValue ] } } }, (err) => {
                if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))

                const getPost = Post.findOne({ _id: id })

                getPost.exec(async (err, post) => {
                    if(err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                    if(!post || post === null) return next(new HttpException(404, "NOT_FOUND", "post not found"))
                    
                    let newPost: any = post

                    await NbLike.findOne({ postId: post._id }, (err, nbLike) => {
                        if (err || nbLike === null) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                        if (nbLike?.userIdLike.includes(`${userId}`)) {
                            newPost.currentUserLiked = true
                        } else {
                            newPost.currentUserLiked = false
                        }
                    })

                    res.status(200).json({ status: "success", message: "Comment added", data: newPost })
                })
            })
           
        })
    })
}

// Route remove comment
export const removeComment = (req: Request, res: Response, next: NextFunction) => {
    const postId = req.params.id 
    const commentId = req.params.commentId
    const { userId } = getUserId

    Post.findOne({ _id: postId }, (err, post) => {
        if(err) return new HttpException(500, "SERVER_ERROR", "Unknow error")
        if(!post) return new HttpException(404, "NOT_FOUND", "post not found")

        let notFound = true
        for (let i = 0; i < post.comments.length; i++) {
            if (post.comments[i].id === commentId && post.comments[i].user.id === userId) {
                notFound = false
                Post.updateOne({ _id: postId }, { $pull: { comments: { id: commentId } } }, (err) => {
                    if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                    res.status(200).json({ status: "success", message: "Comment removed" })
                })
                break
            }
        }
        if (notFound) return next(new HttpException(404, "NOT_FOUND", "Comment not found"))
    })
}

// Route delete post
export const deletePosts = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id

    Post.findOne({ _id: id }, (err, post) => {
        if(err) return new HttpException(500, "SERVER_ERROR", "Unknow error")
        if(!post) return new HttpException(404, "NOT_FOUND", "post not found")

        NbLike.deleteOne({ postId: id },(err) => {
            if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))

            Post.deleteOne({ _id: id }, (err) => {
                if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                if(post?.images != null) {
                    if(post?.images.length > 0) {
                        post?.images.forEach(item => {
                            const removeBaseUrl = new RegExp(BASE_URL, "g")
                            fs.unlinkSync(path.join(__dirname, `..${item.replace(removeBaseUrl, "")}`))
                        })
                    }
                }
                
                res.status(200).json({ status: "success", message: "Post deleted" })
            })
        })
    })
}

// Route get All post of a user
export const getAllUserPosts = (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id

    const allUserPosts = Post.find({ "user.id": id })

    allUserPosts.exec((err, posts) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        res.status(200).json({ status: "success", message: "Get all User posts", data: posts })
    })
}