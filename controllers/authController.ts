import { Request, Response, NextFunction } from "express"
import * as EmailValidator from "email-validator"
import HttpException from "../errors/ErrorFormat"
import jwt from "jsonwebtoken"
import { jwtRefreshTokenSecret, jwtSecret } from "../server"
import User, { IUserDoc } from "../models/User"
import RefreshToken from "../models/RefreshToken"
import { getRandomColor } from "../utils/Utils"
import Team from "../models/Team"

// Route login
export const login = (req: Request, res: Response, next: NextFunction) => {
    const { email, password } = req.body

    if (!EmailValidator.validate(email)) return next(new HttpException(400, "BAD_REQUEST", "Invalid email"))

    User.findOne({ email: email }, (err, user) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        else if (!user) return next(new HttpException(401, "UNAUTHORIZED", "invalid credentials"))

        user!.comparePassword(password, (err: HttpException, match: IUserDoc) => {
            if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
            else if (!match) return next(new HttpException(401, "UNAUTHORIZED", "invalid credentials"))

            const accessToken = jwt.sign({ id: user._id }, jwtSecret, { expiresIn: `${process.env.JWT_EXPIRES_IN}` })
            const refreshToken = jwt.sign({ id: user._id }, jwtRefreshTokenSecret)

            const resfrehTokenModel = new RefreshToken({ refreshToken, id: user._id })

            resfrehTokenModel.save((err, result) => {
                if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                
                RefreshToken.find({ refreshToken: result.refreshToken }, (err, token) => {
                    if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                    res.status(200).json({ status: "success", message: "Login succeeded" , data: { accessToken: accessToken, refreshToken: refreshToken } })  
                })
            })
        })
    })
}

// Route register
export const register = (req: Request, res: Response, next: NextFunction) => {
    const { email, firstName, lastName, team, job, password } = req.body
    const image = `https://eu.ui-avatars.com/api/?background=${getRandomColor()}&color=fff&name=${firstName}+${lastName}&size=512&bold=true`

    if (email === undefined || firstName === undefined || lastName === undefined || password === undefined || job === undefined || team === undefined) return next(new HttpException(400, "BAD_REQUEST", "missing_fields"))
    if (!EmailValidator.validate(email)) {
        return next(new HttpException(400, "BAD_REQUEST", "Invalid email"))
    }

    User.find({ email: email }, (err, existingUser) => {
        if (err) {
            return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        } else if (existingUser.length) {
            return next(new HttpException(409, "CONFLICT", "Email already exists"))
        }

        Team.find({ name: team }, (err, existingTeam) => {
            if (err) {
                return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
            } else if (!existingTeam.length) {
                return next(new HttpException(400, "BAD_REQUEST", "Invalid team"))
            }

            const user = new User({ email, firstName, lastName, team, teamId: existingTeam[0]._id, job, image, password })

            user.save((err, user) => {
                if (err) {
                    return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                }

                User.findById(user._id, { password: 0 }, (err, user) => {
                    if (err) {
                        return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
                    }
                    res.status(201).json({ status: "success", message: "User created" , data: user })
                })
            })
        })
    })
}

// Route refresh token
export const changeToken = (req: Request, res: Response, next: NextFunction) => {
    const { token } = req.body

    if (!token) return next(new HttpException(401, "UNAUTHORIZED", "Token not found"))

    RefreshToken.find({ refreshToken: token }, (err, result) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        if (!result.length) return next(new HttpException(401, "UNAUTHORIZED", "RefreshToken not valid"))

        jwt.verify(token, jwtRefreshTokenSecret, (err: any, user: any) => {
            if (err) return next(new HttpException(401, "UNAUTHORIZED", "Token not valid"))
    
            const accessToken = jwt.sign({ id: user.id }, jwtSecret, { expiresIn: `${process.env.JWT_EXPIRES_IN}` })
            res.status(200).json({ status: "success", message: "Refresh token succeeded" , data: { accessToken: accessToken } })
        })
    })
}

// Route logout
export const logout = (req: Request, res: Response, next: NextFunction) => {
    const { token } = req.body

    jwt.verify(token, jwtRefreshTokenSecret, (err: any, user: any) => {
        if (err) return next(new HttpException(401, "UNAUTHORIZED", "Token not valid"))

        RefreshToken.deleteMany({ id: user.id }, (err) => {
            if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
            res.status(200).json({ status: "success", message: "Logout successful" })
        })
    })
}
