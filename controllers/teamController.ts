import { Request, Response, NextFunction } from "express"
import HttpException from "../errors/ErrorFormat"
import Team from "../models/Team"

// Route all teams
export const allTeams = (req: Request, res: Response, next: NextFunction) => {
    Team.find({}, (err, teams) => {
        if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))

        const renderTeams = teams.map(team => team.name)
        res.status(200).json({ status: "success", message: "Get all Teams", data: renderTeams })
    })
}

// Route add team
export const addTeam = (req: Request, res: Response, next: NextFunction) => {
    const team = req.params.team

    Team.find({ name: team }, (err, existingTeam) => {
        if (err) {
            return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
        } else if (existingTeam.length) {
            return next(new HttpException(409, "CONFLICT", "Team already exists"))
        }

        const newTeam = new Team({ name: team })

        newTeam.save((err) => {
            if (err) return next(new HttpException(500, "SERVER_ERROR", "Unknow error"))
            res.status(201).json({ status: "success", message: "Team created" , data: [] })
        })
    })
}
